package Drive;

public class Car extends Vehicle {
    @Override
    public void startEngine(){
        System.out.println("Car Start");
    }
    @Override
    public void drive(){
        System.out.println("Car Drive");
    }
}
