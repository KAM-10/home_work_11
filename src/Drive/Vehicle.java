package Drive;

abstract class Vehicle {
    public abstract void startEngine();
    public abstract void drive();

}
