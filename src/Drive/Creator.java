package Drive;

public class Creator {
    public static void main(String[] args) {
        Car car = new Car();
        Motorcycle motorcycle = new Motorcycle();
        Truck truck = new Truck();

        car.startEngine();
        car.drive();

        truck.startEngine();
        truck.drive();

        motorcycle.startEngine();
        motorcycle.drive();



    }
}
